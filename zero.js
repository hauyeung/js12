window.onload = init;

function init(){
	var subbtn = document.getElementById('submit');
	subbtn.onclick = calculate;
}

function calculate(){
	var op1 = document.getElementById('operand1').value;
	var operator = document.getElementById('operator').value;
	var op2 = document.getElementById('operand2').value;
	var  res = document.getElementById('result');
	if (op1 == '' || op2 == '' || operator == '')
	{
		alert('Please enter everything.')
	}
	var result = 0;
	try{
		if (operator.trim()=== '+'){		
			result = parseFloat(op1) + parseFloat(op2);
			console.log(result);
			if (isNaN(result) == false){
				res.innerHTML = result;
			}
			else{
				res.innerHTML = 'Operand(s) are not numbers';
			}
			
		}
		else if (operator.trim()=== '-'){
			result = parseFloat(op1) - parseFloat(op2);
			if (isNaN(result) == false){
				res.innerHTML = result;
			}
			else{
				res.innerHTML = 'Operand(s) are not numbers';
			}
		}
		else if (operator.trim()=== '*'){
			result = parseFloat(op1) * parseFloat(op2);
			if (isNaN(result) == false){
				res.innerHTML = result;
			}
			else{
				res.innerHTML = 'Operand(s) are not numbers';
			}
		}
		else if (operator.trim()=== '/'){
			if (parseFloat(op2) == 0){
				throw new Error('Cannot divide by zero');
			}
			else{
				result = parseInt(op1) / parseInt(op2);
				if (isNaN(result) == false){
				res.innerHTML = result;
				}
				else{
					res.innerHTML = 'Operand(s) are not numbers';
				}
			}			
		}
		else{
			throw new Error('Invalid Operator')
		}
		
	}
	catch(e){
		res.innerHTML = e.message;
	}
}
